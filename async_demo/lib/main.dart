import 'package:async_demo/Pages/PackageDemo.dart';
import 'package:flutter/material.dart';
import 'package:async_demo/Pages/home.dart';
import 'package:async_demo/Pages/asyncPage.dart';

void main() {
  runApp(MaterialApp(
      initialRoute: '/package',
      routes: {
      '/home' : (context) => Home(),
      '/async' : (context) => AsyncDemo(),
      '/package': (context) => PackageDemoClass()
    },
  ));
}

