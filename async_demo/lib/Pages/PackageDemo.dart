import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:async_demo/services/world_time.dart';

class PackageDemoClass extends StatefulWidget {
  @override
  _PackageDemoClassState createState() => _PackageDemoClassState();
}

class _PackageDemoClassState extends State<PackageDemoClass> {
  String time ='loading..';
  void getWorldTime() async{
    ShowWorldTime instance = ShowWorldTime(flag:'flag.png',url:'Europe/London');
    await instance.getTime();
    setState(() {
      time = instance.time;
    });
    print('time in loading = $time');
  }

  void getData() async
  {
    String launchurl = 'https://jsonplaceholder.typicode.com/todos/1';
    if(await canLaunch(launchurl))
      {
        print('url launched');
          launch(launchurl);
          //Map data = jsonDecode(response.body);
          //print(data[title]);
      }
    else
      {
        print('url not launched');
      }
  }

  @override
  void initState() {
    super.initState();
    print('PackageDemo init state function');
    //getData();
    getWorldTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome to Package page'),
      ),
      body: Padding(
        padding: EdgeInsets.all(50.0),
        child: Text('$time'),
      ));
  }
}
