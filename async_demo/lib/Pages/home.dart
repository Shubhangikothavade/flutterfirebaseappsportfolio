import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Hi Async Demo'),
        ),
        body: RaisedButton.icon(
            onPressed: (){
              Navigator.pushNamed(context, '/package',);
            },
            icon: Icon(
              Icons.edit_location,
            ),
            label: Text('Click me!')
        )
    );
  }
}