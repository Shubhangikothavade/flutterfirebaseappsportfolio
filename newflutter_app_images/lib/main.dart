import 'package:flutter/material.dart';

void main() {
  runApp(MyClickedIconClass());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Image Demo..'),
          centerTitle: true,
        ),
        body: Image(
          image: AssetImage('assets/fruits.jpg'),
          //image: NetworkImage('https://cdn.wccftech.com/wp-content/uploads/2016/09/spacee-740x463.jpg'),
        ),
      ),
    );
  }
}

class MyIconClass extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Icon and Button'),
        ),
        body:Center(
          child: Icon(
            Icons.airplanemode_active,
            color: Colors.red,
            size: 90.0,
          ),
        )
        )
      );
  }
}


class MyButtonClass extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Icon and Button'),
          ),
          body: Center(
            child: FlatButton(
              onPressed: (){
                print('you clicked me!');
              },
              child: Text('Click Me!'),
              color: Colors.red,
            ),
          )
      ),
    );
  }
}

class MyIconButtonClass extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Icon and Button'),
          ),
          body: Center(
            child: RaisedButton.icon(
              icon: Icon(
                  Icons.mail,
              ),
              onPressed: (){
                print('you clicked me!');
              },
              label: Text('Mail me.'),
              color: Colors.red,
            ),
          )
      ),
    );
  }
}

class MyClickedIconClass extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Icon and Button'),
          ),
          body: Center(
            child: IconButton(
              icon: Icon(
                Icons.mail,
                size: 50.0,
              ),
              onPressed: (){
                print('Clicked on me!');
              },
              color: Colors.amber,
            ),
          )
      ),
    );
  }
}