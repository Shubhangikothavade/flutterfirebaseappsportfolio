import 'dart:convert';
import 'package:http/http.dart';
import 'package:intl/intl.dart';

class ClassWorldTime{
  String location;
  String flag;
  String time;
  String url;

  ClassWorldTime({this.location,this.flag,this.url});

  Future<void> getTime() async
  {
    Response response = await get('http://worldtimeapi.org/api/timezone/$url');
    Map data = jsonDecode(response.body);

    String datetime = (data['datetime']);
    String offset = (data['utc_offset']).substring(1,3);
    DateTime now = DateTime.parse(datetime);
    now = now.add(Duration(hours: int.parse(offset)));
    this.time = DateFormat.jm().format(now);
    print('time = $time');
  }
}