import 'package:flutter/material.dart';
import 'package:world_time_app_project/Pages/Home.dart';
import 'package:world_time_app_project/Pages/Choose_Location.dart';
import 'package:world_time_app_project/Pages/Loading.dart';
import 'package:world_time_app_project/Pages/loadingsecond.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/loading',
    routes: {
      '/home' : (context) => ClassHome(),
      '/location' : (context) => ClassChooseLocation(),
      '/loading' : (context) => ClassLoading(),
      '/loadingSecond' : (context) => ClassLoadingSecond(),

    },
  ));
}