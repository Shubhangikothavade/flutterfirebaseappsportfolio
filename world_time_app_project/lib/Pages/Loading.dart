import 'package:flutter/material.dart';
import 'package:world_time_app_project/Service/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ClassLoading extends StatefulWidget {
  @override
  _ClassLoadingState createState() => _ClassLoadingState();
}

class _ClassLoadingState extends State<ClassLoading> {

  String time = 'loading..';

  void getWorldTime() async{
    ClassWorldTime worldTime = ClassWorldTime(location: 'London',flag: 'day.jpeg',url: 'Europe/London');
    await worldTime.getTime();

    Navigator.pushReplacementNamed(context, '/home',arguments: {
      'location' : worldTime.location,
      'flag' : worldTime.flag,
      'time' : worldTime.time
    });
  }

  @override
  void initState() {
      getWorldTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
      appBar: AppBar(
        title: Text('Loading AppBar'),
      ),
      body: SpinKitRotatingCircle(
        color: Colors.white,
        size: 50.0,
      ),
    );
  }
}
