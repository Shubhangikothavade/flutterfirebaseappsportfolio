import 'package:flutter/material.dart';
import 'package:world_time_app_project/Service/world_time.dart';

class ClassChooseLocation extends StatefulWidget {
  @override
  _ClassChooseLocationState createState() => _ClassChooseLocationState();
}

class _ClassChooseLocationState extends State<ClassChooseLocation> {
  List<ClassWorldTime> worldTime = [
     ClassWorldTime(location: 'London',flag: 'Ayaansh.png',url: 'Europe/London'),
     ClassWorldTime(location: 'Berlin',flag: 'Shubhangi.png',url: 'Europe/Berlin'),
     ClassWorldTime(location: 'Chicago',flag: 'Pushkar.png',url: 'America/Chicago')
  ];

  void updateTime(index) async{
    ClassWorldTime instance = worldTime[index];
    await instance.getTime();

    Navigator.pop(context,{
      'location' : instance.location, // worldTime[index].location,
      'time' :instance.time, // worldTime[index].time,
      'flag' : 'day.jpeg' // worldTime[index].flag
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose_Location appBar..'),
      ),
      body: ListView.builder(
          itemCount: worldTime.length,
          itemBuilder: (context,index){
            return Card(
              child: ListTile(
                title: Text('${worldTime[index].location}'),
                onTap: (){
                  updateTime(index);
                  //print('clicked : ${worldTime[index].location}');
                },
                leading: CircleAvatar(
                  backgroundImage: AssetImage('asset/${worldTime[index].flag}'),
                ),
              ),
            );
          },
      ),
    );
  }
}
