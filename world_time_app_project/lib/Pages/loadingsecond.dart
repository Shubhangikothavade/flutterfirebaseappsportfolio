import 'package:flutter/material.dart';
import 'package:world_time_app_project/Service/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ClassLoadingSecond extends StatefulWidget {
  @override
  _ClassLoadingSecondState createState() => _ClassLoadingSecondState();
}

class _ClassLoadingSecondState extends State<ClassLoadingSecond> {

  String time = 'loading..';

  void getWorldTime() async{
    ClassWorldTime worldTime = ClassWorldTime(location: 'Berlin',flag: 'day.jpeg',url: 'Europe/Berlin');
    await worldTime.getTime();

    Navigator.pushReplacementNamed(context, '/home',arguments: {
      'location' : worldTime.location,
      'flag' : worldTime.flag,
      'time' : worldTime.time
    });
  }

  @override
  void initState() {
    getWorldTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
      appBar: AppBar(
        title: Text('Loading AppBar'),
      ),
      body: SpinKitRotatingCircle(
        color: Colors.white,
        size: 50.0,
      ),
    );
  }
}