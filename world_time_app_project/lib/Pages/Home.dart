import 'package:flutter/material.dart';

class ClassHome extends StatefulWidget {
  @override
  _ClassHomeState createState() => _ClassHomeState();
}

class _ClassHomeState extends State<ClassHome> {
  Map data = {};
  String time;
  String location;
  String flag;

  @override
  Widget build(BuildContext context) {

    data = data.isEmpty ? (ModalRoute.of(context).settings.arguments) : data ;
    time = data['time'];
    location = data['location'];
    flag = data['flag'];

    return Scaffold(
      appBar: AppBar(
        title: Text('Home appBar..'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('asset/$flag'),
            fit: BoxFit.cover
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FlatButton.icon(
                onPressed: () async{
                dynamic result = await Navigator.pushNamed(context, '/location');
                setState(() {
                  data = {
                    'location' : result['location'],
                    'time' : result['time'],
                    'flag' : result['flag'],
                  };
                });
                },
                icon: Icon(
                  Icons.edit_location
                ),
                label: Text('Edit Location',
                style: TextStyle(
                  fontSize: 20.0,
                ),
                )
            ),
            SizedBox(height: 20.0,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('$time',
                  style: TextStyle(
                    fontSize: 50.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0,),
            Text('$location',
              style: TextStyle(
                fontSize: 30.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}