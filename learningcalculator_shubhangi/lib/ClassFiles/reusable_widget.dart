import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:learningcalculator_shubhangi/Constants/all_constants.dart';

class ReusableWidget extends StatelessWidget {

  final Function onPress;
  final Color bgcolor;
  final Widget columnwidget;

  ReusableWidget({this.onPress,this.bgcolor,this.columnwidget});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          color: bgcolor,
        ),
        child: columnwidget,
      ),
    );
  }
}