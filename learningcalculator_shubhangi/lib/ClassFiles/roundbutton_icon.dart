import 'package:flutter/material.dart';
class RoundButtonIcon extends StatelessWidget {
  final Icon bgIcon;
  final Function onClick;

  RoundButtonIcon({this.bgIcon,this.onClick});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onClick,
      child: bgIcon,
      elevation: 6.0,
      fillColor: Colors.black26,
      shape: CircleBorder(),
      constraints: BoxConstraints.tightFor(
        width: 56.0,
        height: 56.0,
      ),
    );
  }
}
