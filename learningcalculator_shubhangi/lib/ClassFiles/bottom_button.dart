import 'package:flutter/material.dart';
import 'package:learningcalculator_shubhangi/Constants/all_constants.dart';
class BottomButton extends StatelessWidget {
  final Widget buttonChild;
  BottomButton({this.buttonChild});

  @override
  Widget build(BuildContext context) {
    return Container(
        color: kMainColor,
        height: 60.0,
        width: double.infinity,
        margin: EdgeInsets.only(top:5.0),
        child: buttonChild
    );
  }
}