import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:learningcalculator_shubhangi/ClassFiles/bottom_button.dart';
import 'package:learningcalculator_shubhangi/Constants/all_constants.dart';
import 'package:learningcalculator_shubhangi/ClassFiles/reusable_widget.dart';

class AnswerPage  extends StatelessWidget {
  final bmiCalculate;
  final interText;
  final result;

  AnswerPage({this.result,this.bmiCalculate,this.interText});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20.0,top:20.0),
                  child: Text(
                    'YOUR RESULT',
                    style: textstyle,
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(20.0),
                  child: ReusableWidget(
                    columnwidget: Column(
                      children: [
                        Text(
                          '$result',
                          style: textstyle,
                        ),
                        SizedBox(height: 30.0,),
                        Text(
                          '$bmiCalculate',
                          style: textstyle,
                        ),
                        SizedBox(height: 30.0,),
                        Text(
                          '$interText',
                          style: textstyle,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
                BottomButton(
                  buttonChild: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'RE-CALCULATE',
                        style: textstyle,
                      )),
                )
              ],
            ),
          ),
        ));
  }
}