import 'package:flutter/material.dart';
import 'package:learningcalculator_shubhangi/ClassFiles/reusable_widget.dart';
import 'package:learningcalculator_shubhangi/ClassFiles/roundbutton_icon.dart';
import 'package:learningcalculator_shubhangi/Components/answer_page.dart';
import 'package:learningcalculator_shubhangi/Constants/all_constants.dart';
import 'package:learningcalculator_shubhangi/ClassFiles/bottom_button.dart';
import 'package:learningcalculator_shubhangi/Components/answer_page.dart';
import 'package:learningcalculator_shubhangi/ClassFiles/bmi_calculator.dart';

class CalculatorPage extends StatefulWidget {
  @override
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  double varChangedval = 120.0;
  Color bgColorMale = kMainColor;
  Color bgColorFemale = kMainColor;
  int weightnum = 60;
  int agenum = 30;

  void changeColor(String str) {
    if (str == 'Male') {
      bgColorMale = Colors.black87;
      bgColorFemale = kMainColor;
    } else {
      bgColorMale = kMainColor;
      bgColorFemale = Colors.black87;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text('BMI CALCULATOR')),
        ),
        body: SafeArea(
          child: Column(
              children: [
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: ReusableWidget(
                      columnwidget: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.check),
                          SizedBox(height: 20.0),
                          Text('Male', style: textstyle)
                        ],
                      ),
                      onPress: () {
                        setState(() {
                          changeColor('Male');
                        });
                      },
                      bgcolor: bgColorMale,
                    ),
                  ),
                  Expanded(
                    child: ReusableWidget(
                      columnwidget: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.add),
                          SizedBox(height: 20.0),
                          Text('Female', style: textstyle)
                        ],
                      ),
                      onPress: () {
                        setState(() {
                          changeColor('Female');
                        });
                      },
                      bgcolor: bgColorFemale,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: ReusableWidget(
                      columnwidget: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'HEIGHT',
                            style: textstyle,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            '${varChangedval.toInt()} cm',
                            style: textstyle,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          SliderTheme(
                            data: SliderTheme.of(context).copyWith(
                              activeTrackColor: Colors.pink,
                              inactiveTrackColor: Colors.black38,
                              overlayColor: Colors.pink,
                              thumbColor: Colors.blue,
                            ),
                            child: Slider(
                              value: varChangedval,
                              min: 100.0,
                              max: 200.0,
                              onChanged: (value) {
                                setState(() {
                                  varChangedval = value;
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      bgcolor: kMainColor,
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: ReusableWidget(
                      columnwidget: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('WEIGHT', style: textstyle),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text('$weightnum', style: textstyle),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                RoundButtonIcon(
                                  bgIcon: Icon(Icons.add),
                                  onClick: () {
                                    setState(() {
                                      weightnum++;
                                    });
                                  },
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                RoundButtonIcon(
                                  bgIcon: Icon(Icons.remove),
                                  onClick: () {
                                    setState(() {
                                      weightnum--;
                                    });
                                  },
                                )
                              ]),
                        ],
                      ),
                      bgcolor: kMainColor,
                    ),
                  ),
                  Expanded(
                    child: ReusableWidget(
                      columnwidget: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('AGE', style: textstyle),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text('$agenum', style: textstyle),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                RoundButtonIcon(
                                  bgIcon: Icon(Icons.add),
                                  onClick: () {
                                    setState(() {
                                      agenum++;
                                    });
                                  },
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                RoundButtonIcon(
                                  bgIcon: Icon(Icons.remove),
                                  onClick: () {
                                    setState(() {
                                      agenum--;
                                    });
                                  },
                                )
                              ]),
                        ],
                      ),
                      bgcolor: kMainColor,
                    ),
                  ),
                ],
              ),
            ),
            BottomButton(
              buttonChild: FlatButton(
                  onPressed: () {
                    CalculatorBrain cal = CalculatorBrain(height:varChangedval.toInt(),weight: weightnum);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context)
                        => AnswerPage(
                          bmiCalculate: cal.calculateBMI(),
                          result: cal.getResult(),
                          interText: cal.getInterpretation(),
                        )));
                  },
                  child: Text('CALCULATE',
                    style: textstyle,)),

            )
          ]),
        ));
  }
}