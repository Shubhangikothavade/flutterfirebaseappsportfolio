import 'package:flutter/material.dart';
import 'CustomListClass.dart';

void main() {
  runApp(MaterialApp(
    home: ListOfDtaToShow(),
  ));
}

class ListOfDtaToShow extends StatefulWidget {
  @override
  _ListOfDtaToShowState createState() => _ListOfDtaToShowState();
}

/*class _ListOfDtaToShowState extends State<ListOfDtaToShow> {
  List<String> ListOfDemo = [
    'List 1',
    'List 2',
    'List 3'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Demo'),
        centerTitle: true,
      ),
      body: Column(
        children: ListOfDemo.map((e) => Text(e,
        style: TextStyle(
          letterSpacing: 2.0,
          fontSize: 20.0,
        ),)).toList(),

      ),
    );
  }
}*/

class _ListOfDtaToShowState extends State<ListOfDtaToShow> {
  List<ListClass> ListOfDemo = [
    ListClass(quote: 'List 1',author: 'author 1'),
    ListClass(quote: 'List 2',author: 'author 2'),
    ListClass(quote: 'List 3',author: 'author 3'),
    /*ListClass('List 1','author 1'),
    ListClass('List 2','author 2'),
    ListClass('List 3','author 4'),*/
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Demo'),
        centerTitle: true,
      ),
      body: Column(
        //crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: ListOfDemo.map((e) => Text('${e.quote} - ${e.author}',
          style: TextStyle(
            letterSpacing: 2.0,
            fontSize: 20.0,
          ),)).toList(),
      ),
    );
  }
}

