import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(MyIdApp());
}

class MyIdApp extends StatefulWidget{
  @override
  _MyIdAppState createState() => _MyIdAppState();
}

class _MyIdAppState extends State<MyIdApp> {
  int age = 30;

  @override
  Widget build(BuildContext context) {
   return MaterialApp(
     home: Scaffold(
       floatingActionButton: FloatingActionButton(
         onPressed: (){
           setState(() {
             age = age + 1;
           });
         },
         child: Icon(
           Icons.add,
         ),
       ),
       backgroundColor: Colors.deepPurpleAccent,
       appBar: AppBar(
         title: Text('First App From Ninja Id'),
         backgroundColor: Colors.green,
         elevation: 0.0,
       ),

       body: Padding(
         padding: EdgeInsets.fromLTRB(40.0,40.0,40.0,40.0),
         child: Column(
           crossAxisAlignment: CrossAxisAlignment.start,
           children: <Widget>[
             Center(
               child: CircleAvatar(
                 backgroundImage: AssetImage('assets/redflower.jpg'),
                 radius: 40.0,
               ),
             ),
             Divider(
               color: Colors.amberAccent,
               height: 60.0,
             ),
             Text(
                 'Shubhangi',
               style: TextStyle(
                 fontSize: 30.0,
                 color: Colors.red,
                 letterSpacing: 2.0,
                 fontWeight: FontWeight.bold
               ),
             ),
            SizedBox(height: 20.0,),
             Row(
               children : <Widget>[
                 Text(
                   'Pushkar',
                   style: TextStyle(
                       fontSize: 30.0,
                       color: Colors.cyan,
                       letterSpacing: 2.0
                   ),
                 ),
                 SizedBox(width: 20.0,),
                 Text(
                     '$age',
                   style: TextStyle(
                     fontSize: 30.0,
                     color: Colors.cyan,
                 ),
                 )
               ]
             ),
             SizedBox(height: 20.0,),
             Row(
               children: <Widget>[
                 Text(
                   'Ayaansh',
                   style: TextStyle(
                       fontSize: 30.0,
                       color: Colors.green,
                       letterSpacing: 2.0
                   ),
                 ),
                 SizedBox(width: 20.0,),
                 Icon(
                   Icons.account_balance,
                   color: Colors.green,
                   size: 30.0,
                 )
               ],
             ),
           ],
         ),
       ),
     ),
   );
  }
}