import 'package:flutter/material.dart';
import 'CustomList.dart';
import 'CardTemplateFile.dart';

void main() {
  runApp(ShowListOfData());
}

class ShowListOfData extends StatefulWidget {
  @override
  _ShowListOfDataState createState() => _ShowListOfDataState();
}

class _ShowListOfDataState extends State<ShowListOfData> {

  List<ListOfData> list = [
    ListOfData(quote: 'List 11',author: 'author 11'),
    ListOfData(quote: 'List 12',author: 'author 12'),
    ListOfData(quote: 'List 13',author: 'author 13'),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home : Scaffold(
        appBar: AppBar(
          title: Text('Card template demo'),
        ),
        body: Column(
          children: list.map((e) => ExtractQuoteCardTemplate(
              equote1:e,
              deleteRow: (){
                setState(() {
                  list.remove(e);
                });
              },
          )).toList(),
      ),
    ));
  }
}


