import 'package:flutter/material.dart';
import 'CustomList.dart';

class ExtractQuoteCardTemplate extends StatelessWidget {
  final ListOfData equote1;
  final Function deleteRow;
  ExtractQuoteCardTemplate({this.equote1, this.deleteRow});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(20.0,20.0,20.0,10.0),
      color: Colors.pinkAccent,
      child: Padding(
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
        child: Row(
          children: <Widget>[
            Text(
              equote1.quote,
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.cyan
              ),
            ),
            SizedBox(width: 20.0,),
            Text(
                equote1.author,
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.cyan
                )),

            SizedBox(width: 20.0,),
            FlatButton.icon(
                onPressed: deleteRow,
                icon: Icon(Icons.delete),
                label: Text('delete row'),
            )
          ],
        ),
      ),
    );
  }
}