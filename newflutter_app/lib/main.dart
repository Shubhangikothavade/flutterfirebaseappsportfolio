import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: ExpandedWidget(),
  ));
}

// StatelessWidget for hot reload
class Home extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome to scaffold appbar'),
        backgroundColor: Colors.redAccent,
      ),
      body:
        Padding(
          padding: EdgeInsets.all(50.0),
          child: Text(
            'Welcome to scaffold body',
            style: TextStyle(
              fontSize: 20.0,
              decoration: TextDecoration.lineThrough,
            ),
          ),
        ),

      floatingActionButton: FloatingActionButton(
        child: Text('Click'),
        backgroundColor: Colors.redAccent,
        onPressed: (){},
      ),
      backgroundColor: Colors.amber,
    );
    throw UnimplementedError();
  }

}

//padding : inside container around text
//margin : outside around container
class ContainerExample extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome to scaffold appbar'),
        backgroundColor: Colors.redAccent,
      ),
      body: Container(
        padding: EdgeInsets.all((20.0)),
        margin: EdgeInsets.all(30.0),
        child: Text('Hello'),
        color: Colors.cyan,
      ),
    );
  }
}

class RowExample extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome to scaffold appbar'),
        backgroundColor: Colors.redAccent,
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Hello World'),
          RaisedButton(onPressed: null,
          child: Text('Button'),
          ),
          Container(
            padding: EdgeInsets.all(30.0),
            color: Colors.cyan,
            child: Text('Container Widget'),
          )
        ],
      )
    );
  }
}

class ColumnExample extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Welcome to scaffold appbar'),
          backgroundColor: Colors.redAccent,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text('Row 1'),
                Text('Row 2'),
              ],
            ),
            Text('Hello World'),
            RaisedButton(onPressed: null,
              child: Text('Button'),
            ),
            Container(
              padding: EdgeInsets.all(30.0),
              color: Colors.cyan,
              child: Text('Container Widget'),
            )
          ],
        )
    );
  }
}

class ExpandedWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Welcome to scaffold appbar'),
          backgroundColor: Colors.redAccent,
        ),
        body: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(10.0),
                color: Colors.deepPurpleAccent,
                child: Text('1')
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(10.0),
                color: Colors.deepOrange,
                child: Text('2')
          ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(10.0),
                color: Colors.cyan,
                child: Text('3'),
              ),
            )
          ],
        )
    );
  }
}