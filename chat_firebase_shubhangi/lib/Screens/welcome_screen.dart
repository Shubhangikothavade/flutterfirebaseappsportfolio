import 'package:flutter/material.dart';
import 'package:chat_firebase_shubhangi/Screens/login_screen.dart';
import 'package:chat_firebase_shubhangi/Screens/registration_screen.dart';
import 'package:chat_firebase_shubhangi/Classes/all_buttons.dart';

class WelcomeScreen extends StatefulWidget {
  static const String id = 'Welcome_Screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controler_Welcome;
  Animation animationctrl;

  @override
  void initState() {
    super.initState();
    controler_Welcome = AnimationController(
      duration: Duration(seconds: 1),
      vsync: this,
     // upperBound: 100.0
    );

    animationctrl = CurvedAnimation(parent: controler_Welcome,curve: Curves.decelerate);
    controler_Welcome.forward();
    //controler_Welcome.reverse(from: 1.0);
    controler_Welcome.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    controler_Welcome.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white30.withOpacity(controler_Welcome.value),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                Hero(
                  tag: 'logo',
                  child: Container(
                    child: ImageIcon(
                      AssetImage('asset/logo.png'),
                      size: 90.0,
                      color: Colors.red,
                    ),
                   height: 60.0,
                   // height: animationctrl.value * 100,
                  ),
                ),
                Text('Flash Chat',
                  //'${controler_Welcome.value.toInt()}%',
                  style: TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              height: 50.0,
            ),
            Container(
              padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 5.0),
              //margin: EdgeInsets.all(30.0),
              child: AllButtons(
                onClick: () {
                  Navigator.pushNamed(context, RegistrationScreen.id);
                },
                text: 'Registration',
              ),
            ),
            Container(
                padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0),
                child: MaterialButton(
                  splashColor: Colors.yellow,
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                  height: 40.0,
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, LoginScreen.id);
                  },
                ))
          ],
        ),
      ),
    );
  }
}
