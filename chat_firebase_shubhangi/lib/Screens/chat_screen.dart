import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final _fireStore = FirebaseFirestore.instance;
User loggedinUser;

class ChatScreen extends StatefulWidget {
  static const String id = 'Chat_Screen';

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final _auth = FirebaseAuth.instance;
  String typedMessage;


  final messageController = TextEditingController();

  void getUserDetails() {
    try {
      final user = _auth.currentUser;
      loggedinUser = user;
      if (user != null) {
        print('user = ${user.email}');
      }
    } catch (e) {
      print(e);
    }
  }

  void getMessageStream() async
  {
   await for(var snapshot in _fireStore.collection('messages').snapshots()) {
      for(var messages in snapshot.docs)
        {
          print(messages.data());
        }
    }
  }

  @override
  void initState() {
    getUserDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 20.0,
        leading: Icon(
          Icons.arrow_back_ios,
          color: Colors.white,
        ),
        title: Container(
          margin: EdgeInsets.only(left: 80.0),
          height: 40.0,
          width: 400.0,
          //color : Colors.red,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset('asset/logo.png'),
                  Text('Chat'),
                ],
              ),
              IconButton(
                 icon : Icon(Icons.close),
                onPressed: (){
                 // getMessageStream();
                   _auth.signOut();
                   Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MessageStream(),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width:250.0,
                    child: TextField(
                      controller: messageController,
                      textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black
                        ),
                        decoration: InputDecoration(
                      hintText: 'Enter text here',
                    ),
                      onChanged: (value){
                        typedMessage = value;
                      },
                    ),
                  ),
                  FlatButton(
                      onPressed: (){
                        messageController.clear();
                        _fireStore.collection('messages').add(
                          {
                            'text' : typedMessage,
                            'email' : loggedinUser.email
                          }
                        );
                      },
                      child: Text('Send'),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                    color: Colors.blue,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MessageStream extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _fireStore.collection('messages').snapshots(),

        builder: (context,snapshot){
          List<MessageBubble> messagesList = [];
          if(!snapshot.hasData) {
            return Center(
                child : CircularProgressIndicator(
                  backgroundColor: Colors.blueAccent,
                )
            );
          }
          final messageData = snapshot.data.docs.reversed;
          for (var message in messageData) {
            final messageMap = message.data();

            final newmessageText = messageMap['text'];
            final newmessageSender = messageMap['email'];

            final currentUser = loggedinUser.email;

            final messageWidget = MessageBubble(text: newmessageText,
              email: newmessageSender,
            isMe: currentUser == newmessageSender);

            messagesList.add(messageWidget);
          }

          return Expanded(
            child: ListView(
              reverse: true,
              padding: EdgeInsets.symmetric(vertical: 20.0,horizontal: 10.0),
              children: messagesList,
            ),
          );
        }
    );
  }
}

class MessageBubble extends StatelessWidget {
  final text;
  final email;
  final isMe;
  MessageBubble({this.email,this.text,this.isMe});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Text(email),
          Material(
            color: isMe ? Colors.blueAccent : Colors.yellow,
            elevation: 5.0,
            borderRadius: isMe ? BorderRadius.only(
              bottomLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
              topLeft: Radius.circular(15.0)) : BorderRadius.only(
            bottomLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
              topRight: Radius.circular(15.0)),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0,horizontal: 20.0),
              child: Text(text,
                style: TextStyle(
                    fontSize: 20.0,
                  color: Colors.black
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
