import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:chat_firebase_shubhangi/Screens/chat_screen.dart';
//import 'package:chat_firebase_shubhangi/Classes/all_buttons.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class LoginScreen extends StatefulWidget {
  static const String id = 'Login_Screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _auth = FirebaseAuth.instance;
  bool showSpinner = false;
  bool checkval = true;
  Icon imageicon = Icon(Icons.security);

  TextEditingController _emailAddress = TextEditingController();
  TextEditingController _emailPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Hero(
                tag: 'logo',
                child: Container(
                  child: Image.asset(
                    'asset/logo.png',
                  ),
                  height: 100.0,
                ),
              ),
              SizedBox(height: 10.0),
              TextField(
                controller: _emailAddress,
                keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                hintText: 'Enter user id.',
                //labelText: 'Name',
                helperText: 'helper text',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(65.0)),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(65.0)),
                    borderSide: BorderSide(width: 2.0, color: Colors.blue)),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 2.0, color: Colors.grey[900]),
                    borderRadius: BorderRadius.all(Radius.circular(30.0))),
                filled: true,
                fillColor: Colors.grey,
                // fillColor: Colors.yellow
              )),
              SizedBox(height: 10.0),
              TextField(
                controller: _emailPassword,
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Enter Password.',
                    /*suffix: IconButton(
                      icon: imageicon,
                      onPressed: () {
                        setState(() {
                          if (checkval) {
                            print('if');
                            imageicon = Icon(Icons.security);
                            checkval = false;
                          } else {
                            print('else');
                            imageicon = Icon(Icons.remove_red_eye);
                            checkval = true;
                          }
                        });
                      },
                    ),*/
                    helperText: 'Password should be at least 6 characters.',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(65.0)),
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(65.0)),
                        borderSide: BorderSide(width: 2.0, color: Colors.blue)),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 2.0, color: Colors.grey[900]),
                        borderRadius: BorderRadius.all(Radius.circular(30.0))),
                    filled: true,
                    fillColor: Colors.grey,
                    // fillColor: Colors.yellow
                  )),
              FlatButton(
                    onPressed: () async {
                      setState(() {
                        showSpinner=true;
                      });
                      try {
                        final user = await _auth.signInWithEmailAndPassword(
                            email: _emailAddress.text,
                            password: _emailPassword.text);
                        if (user != null)
                          Navigator.pushNamed(context, ChatScreen.id);

                        setState(() {
                          showSpinner=false;
                        });
                      }
                      catch(e)
                      {
                        print('login catch $e');
                      }
                    },
                  child: Text('Login'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
