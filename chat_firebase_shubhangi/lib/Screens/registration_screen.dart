import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:chat_firebase_shubhangi/Screens/chat_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class RegistrationScreen extends StatefulWidget {
  static const String id = 'Registration_Screen';
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  bool showSpinner = false;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String email;
  String password;

  @override
  // void initState() {
  //   super.initState();
  //
  //   //Firebase.initializeApp().whenComplete(() {
  //     print("completed");
  //     setState(() {});
  //   });
 // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        child: Padding(padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Hero(
                tag: 'logo',
                child: Container(
                  child: Image.asset(
                    'asset/logo.png',
                  ),
                  height: 100.0,
                ),
              ),
              SizedBox(height: 10.0),
              TextField(
                onChanged: (value){
                  email=value;
                },
                  //controller: _emailAddress,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    hintText: 'Enter user id.',
                    //labelText: 'Name',
                    helperText: 'helper text',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(65.0)),
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(65.0)),
                        borderSide: BorderSide(width: 2.0, color: Colors.blue)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 2.0, color: Colors.grey[900]),
                        borderRadius: BorderRadius.all(Radius.circular(30.0))),
                    filled: true,
                    fillColor: Colors.grey,
                    // fillColor: Colors.yellow
                  )),
              SizedBox(height: 10.0),
              TextField(
                  onChanged: (value){
                    password = value;
                  },
                  //controller: _emailPassword,
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Enter Password.',

                    helperText: 'Password should be at least 6 characters.',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(65.0)),
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(65.0)),
                        borderSide: BorderSide(width: 2.0, color: Colors.blue)),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                        BorderSide(width: 2.0, color: Colors.grey[900]),
                        borderRadius: BorderRadius.all(Radius.circular(30.0))),
                    filled: true,
                    fillColor: Colors.grey,
                    // fillColor: Colors.yellow
                  )),
              FlatButton(
                onPressed: () async{
                 setState(() {
                   showSpinner = true;
                 });
                  try {
                    print('flat button user = $email');
                    final user = await _auth.createUserWithEmailAndPassword(
                        email: email.trim(), password: password);
                    if (user != null) {
                      Navigator.pushNamed(context, ChatScreen.id);
                    }
                    setState(() {
                      showSpinner = false;
                    });
                  }
                  catch(e){
                    print('in catch');
                    print(e.toString());
                  }
                },
                child: Text('Register'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
