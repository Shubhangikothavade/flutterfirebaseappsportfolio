import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:chat_firebase_shubhangi/Screens/chat_screen.dart';
import 'package:chat_firebase_shubhangi/Screens/login_screen.dart';
import 'package:chat_firebase_shubhangi/Screens/registration_screen.dart';
import 'package:chat_firebase_shubhangi/Screens/welcome_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp();
  runApp(MaterialApp(
    theme: ThemeData().copyWith(
        textTheme: TextTheme(
          body1: TextStyle(
            color: Colors.green,
          )
        )
    ),
    initialRoute: WelcomeScreen.id,
    routes: {
      WelcomeScreen.id : (context) => WelcomeScreen(),
      LoginScreen.id : (context) => LoginScreen(),
      RegistrationScreen.id : (context) => RegistrationScreen(),
      ChatScreen.id : (context) => ChatScreen(),
    },
  ));
}