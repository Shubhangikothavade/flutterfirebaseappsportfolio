import 'package:flutter/material.dart';

class AllButtons extends StatelessWidget {
  final String text;
  final Function onClick;

  AllButtons({this.text, this.onClick});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      splashColor: Colors.yellow,
      color: Colors.blue,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      height: 40.0,
      child: Text(
        '$text',
        style: TextStyle(
          fontSize: 20.0,
        ),
      ),
      //splashColor: Colors.blue,

      onPressed: onClick,
    );
  }
}