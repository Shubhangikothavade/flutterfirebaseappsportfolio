import 'package:flutter/material.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  void initState() {
    print('Loading init..');
  }
  @override
  Widget build(BuildContext context) {
    print('Loading build..');
    return Scaffold(
      body: SafeArea(
          child: Text('Loading screen'),
      ),
    );
  }
}
