import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    print('Home init..');
  }
  @override
  Widget build(BuildContext context) {
    print('Home build..');

    return Scaffold(
      appBar: AppBar(
        title: Text('Home Appbar..'),
      ),
      body: SafeArea(
          child: Column(
            children: <Widget>[
              FlatButton.icon(
                  onPressed: (){
                    Navigator.pushNamed(context, '/location');
                    /*setState(() {
                      print('Home setstate');
                    });*/
                  },
                  icon: Icon(
                    Icons.edit_location,
                  ),
                  label: Text('Edit Home'),
              )
            ],
          )
      ),
    );
  }
}
