import 'package:flutter/material.dart';

class ChooseLocation extends StatefulWidget {
  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {
  @override
  void initState() {
    print('ChooseLocation init..');
  }
  @override
  Widget build(BuildContext context) {
    print('ChooseLocation build..');

    return Scaffold(
      appBar: AppBar(
        title: Text('Location screen AppBar'),
        backgroundColor: Colors.blue[900],
        centerTitle: true,
      ),
      body: FlatButton.icon(
        onPressed: (){
          Navigator.pushNamed(context, '/home');
          setState(() {
                      print('ChooseLocation setstate');
                    });
        },
        icon: Icon(
          Icons.edit_location,
        ),
        label: Text('Edit ChooseLocation'),
      )
      //Text('Choose location screen'),
    );
  }
}
