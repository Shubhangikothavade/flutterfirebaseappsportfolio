import 'package:flutter/material.dart';
import 'package:routeing_between_pages/Pages/choose_location.dart';
import 'package:routeing_between_pages/Pages/home.dart';
import 'package:routeing_between_pages/Pages/loading.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/home',
    routes: {
      '/loading' : (context) => Loading(),
      '/home' : (context)  => Home(),
      '/location' : (context) => ChooseLocation(),
    },
  ));
}