import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// 1. Container background image
// 2. Set background image, color using variable

void main() {
  runApp(MaterialApp(
    home: HomeState(),
  ));
}

class HomeState extends StatefulWidget {
  @override
  _HomeStateState createState() => _HomeStateState();
}

class _HomeStateState extends State<HomeState> {
  @override
  Widget build(BuildContext context) {

    String bgimage = ('hi' == 'hi') ? 'day.jpeg' : 'night.jpg';
    Color bgcolor = Colors.limeAccent;

    return Scaffold(
      appBar: AppBar(
        title: Text('Hi there!',
          style: TextStyle(
              color: bgcolor
          ),),
      ),
      body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('asset/$bgimage'),
                fit: BoxFit.cover,
              )
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('hi',
                style: TextStyle(
                    color: bgcolor,
                    fontSize: 50.0
                ),),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Hi Everyone!',
                    style: TextStyle(
                        color: bgcolor,
                        fontSize: 50.0
                    ),
                  ),
                ],
              )
            ],
          )
      ),
    );
  }
}