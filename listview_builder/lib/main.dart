import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:listview_builder/ListOfEmployee.dart';

// 1. Container background image
// 2. Set background image, color using variable

void main() {
  runApp(MaterialApp(
    home: HomeState(),
  ));
}

class HomeState extends StatefulWidget {
  @override
  _HomeStateState createState() => _HomeStateState();
}

class _HomeStateState extends State<HomeState> {
  List<ClassListOfEmp> empList = [
    ClassListOfEmp(name:'Ayaansh',age:5,gender: 'Male',image: 'Ayaansh.png'),
    ClassListOfEmp(name:'Shubahngi',age:32,gender: 'Female',image: 'Shubhangi.png'),
    ClassListOfEmp(name:'Pushkar',age:35,gender: 'Male',image: 'Pushkar.png'),
  ];



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Hi there!',
        style: TextStyle(
          color: Colors.limeAccent
        ),),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10.0,10.0,10.0,10.0),
        child: Container(
          color: Colors.red,
          child: ListView.builder(
            itemCount: empList.length,
            itemBuilder: (context, index){
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 1.0,horizontal: 2.0),
                child: Card(
                  child: ListTile(
                    title: Text('${empList[index].name}'),
                    leading: CircleAvatar(
                    backgroundImage: AssetImage('asset/${empList[index].image}'),
                    ),
                  onTap: (){
                      print('${empList[index].name} clicked');
                  },
                    tileColor: Colors.limeAccent,
                )
                ),
              );


              /*return Row(
                children: [
                  Text('${empList[index].name}',
                  style: TextStyle(
                    fontSize: 30.0
                  ),),
                  SizedBox(width: 30.0,),
                  Text('${empList[index].age}',
                    style: TextStyle(
                        fontSize: 30.0
                    ),),
                ],
              ); */
            }
          ),
        ),
      )
      );
  }
}