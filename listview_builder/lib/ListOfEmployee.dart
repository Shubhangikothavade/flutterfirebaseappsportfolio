class ClassListOfEmp{
  String name;
  int age;
  String gender;
  String image;

  ClassListOfEmp({this.name,this.age,this.gender,this.image});
}

/*

class _HomeStateState extends State<HomeState> {
  List<ClassListOfEmp> empList = [
    ClassListOfEmp(name:'Ayaansh',age:5,gender: 'Male',image: 'Ayaansh.png'),
    ClassListOfEmp(name:'Shubahngi',age:32,gender: 'Female',image: 'Shubhangi.png'),
    ClassListOfEmp(name:'Pushkar',age:35,gender: 'Male',image: 'Pushkar.png'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hi there!'),
      ),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('asset/night.jpg'),
                fit: BoxFit.cover
            )),
        child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0.0,120.0,0.0,0.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('hi',
                    style: TextStyle(
                      fontSize: 40.0,
                      color: Colors.white
                    ),
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('bye',
                        style: TextStyle(
                            fontSize: 40.0,
                            color: Colors.white
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
      ),
      );
  }
}
 */