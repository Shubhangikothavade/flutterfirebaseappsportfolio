import 'package:flutter/material.dart';

class RowColumn extends StatefulWidget {
  @override
  _RowColumnState createState() => _RowColumnState();
}

class _RowColumnState extends State<RowColumn> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('RowColumn'),
      ),
      body: Column(
        children: [
          // below code shows fit to size when rotate
          Container(
            margin: EdgeInsets.all(20),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    //margin: EdgeInsets.all(20),
                    height: 50,
                    width: 170,
                    color: Colors.pink,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    //margin: EdgeInsets.all(20),
                    height: 50,
                    width: 170,
                    color: Colors.pink,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    //margin: EdgeInsets.all(20),
                    height: 50,
                    width: 170,
                    color: Colors.pink,
                  ),
                ),
              ],
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(30.0),
            child: Container(
              padding: EdgeInsets.all(20),
              height: 100,
              width: 200,
              color: Colors.yellow,
              child: Text('ClipRRect Widget'),
              alignment: Alignment.bottomCenter,
            ),
          ),
          // below code shows ListView
          Container(
            margin: EdgeInsets.all(20),
            height: 300,
            width: 300,
            color: Colors.grey,
            child: ListView(
              children: [
                Container(
                  height: 50,
                  color: Colors.red,
                  child: Text('ListView'),
                  alignment: Alignment.center,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  //width: 30,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  //width: 30,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  //width: 30,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  //width: 30,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  //width: 30,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  //width: 30,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  //width: 30,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  //width: 30,
                  color: Colors.red,
                ),
              ],
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          print('pressed');
        },
      ),
      bottomNavigationBar: BottomAppBar(
        clipBehavior: Clip.antiAlias,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
          color: Colors.blue,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 0;
                  });
                  print('Home clicked : $_currentIndex');
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.home,
                      size: 30,
                    ),
                    Text('Home'),
                  ],
                ),
              ),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 1;
                  });
                  print('Search clicked : $_currentIndex');
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.search,
                      size: 30,
                    ),
                    Text('Search'),
                  ],
                ),
              ),
              SizedBox.shrink(),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 2;
                  });
                  print('Camera clicked : $_currentIndex');
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.camera,
                      size: 30,
                    ),
                    Text('Camera'),
                  ],
                ),
              ),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 3;
                  });
                  print('Profile clicked : $_currentIndex');
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.person,
                      size: 30,
                    ),
                    Text('Profile'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

/*
other way to use bottomNavigationBar
child: Container(
          height: 50.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(
                Icons.home,
                size: 40,
              ),
              Icon(
                Icons.search,
                size: 40,
              ),
              SizedBox.shrink(),
              Icon(
                Icons.camera,
                size: 40,
              ),
              Icon(
                Icons.person,
                size: 40,
              ),
            ],
          ),
          //color: Colors.blue,
        ),
 */
/*
 bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        iconSize: 30,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
              backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: 'Search',
              backgroundColor: Colors.blue
          ),

          BottomNavigationBarItem(
              icon: Icon(Icons.camera),
              label: 'Camera',
              backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
              backgroundColor: Colors.blue
          ),
        ],
        onTap: (index){
          setState(() {
            _currentIndex = index;
          });
        },
      ),
 */
