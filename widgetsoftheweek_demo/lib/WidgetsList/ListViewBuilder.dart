import 'package:flutter/material.dart';

class ListViewBuilderClass extends StatefulWidget {
  @override
  _ListViewBuilderClassState createState() => _ListViewBuilderClassState();
}

class _ListViewBuilderClassState extends State<ListViewBuilderClass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          iconSize: 20.0,
        ),
      ),
    );
  }
}
