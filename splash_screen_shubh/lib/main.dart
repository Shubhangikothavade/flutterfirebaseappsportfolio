import 'package:flutter/material.dart';
import 'detail_menu.dart';

void main() {
  runApp(MaterialApp(
    home: MyNutritionApp(),
  ));
}

class MyNutritionApp extends StatefulWidget {
  @override
  _MyNutritionAppState createState() => _MyNutritionAppState();
}

class _MyNutritionAppState extends State<MyNutritionApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan,
      body: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: () {}),
              Container(
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.filter_list,
                        ),
                        onPressed: () {}),
                    IconButton(
                        icon: Icon(
                          Icons.menu,
                        ),
                        onPressed: () {}),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0),
            child: Row(
              children: [
                Text(
                  'Healthy',
                  style: TextStyle(
                    fontSize: 40.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(width: 10.0),
                Text(
                  'Food',
                  style: TextStyle(
                    fontSize: 40.0,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 40.0,
          ),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            height: MediaQuery.of(context).size.height - 250,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius:
                    BorderRadius.only(topLeft: Radius.circular(50.0))),
            child: ListView(
              scrollDirection: Axis.vertical,
              children: [
                _FoodItem('FoodPlate1.jpg', 'Paneer Pasanda', 'Rs 300'),
                _FoodItem('FoodPlate2.jpg', 'Pani Poori', 'Rs 30'),
                _FoodItem('FoodPlate3.jpg', 'Pav Bhaji', 'Rs 100'),
                _FoodItem('FoodPlate4.jpg', 'Shev Puri', 'Rs 40'),
                _FoodItem('FoodPlate5.jpg', 'Idli Sambar', 'Rs 80'),
              ],
            ),
          ),
          Container(
              height: MediaQuery.of(context).size.height - 610,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 2.0),
                          borderRadius: BorderRadius.circular(7.0)
                          ),
                      child: IconButton(
                        icon: Icon(Icons.search),
                        iconSize: 40.0,
                        onPressed: () {},
                        color: Colors.black,
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 2.0),
                          borderRadius: BorderRadius.circular(7.0)
                          ),
                      child: IconButton(
                        icon: Icon(Icons.shopping_cart),
                        iconSize: 40.0,
                        onPressed: () {},
                        color: Colors.black,
                      ),
                    ),
                    Container(
                      child: FlatButton(
                        height: 60.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Text(
                          'CheckOut',
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () {},
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              )),
        ],
      ),
    );
  }

  Widget _FoodItem(String image, String foodName, String price) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, top: 20.0),
      child: InkWell(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(
              builder: (BuildContext context)
              => DetailMenu(foodName: foodName, foodPrice: price,foodImage: image,)));
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CircleAvatar(
                    backgroundImage: AssetImage('asset/$image'),
                    radius: 50.0,
                  ),
                  SizedBox(
                    width: 30.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '$foodName',
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '$price',
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            IconButton(icon: Icon(Icons.add), onPressed: () {})
          ],
        ),
      ),
    );
  }
}
