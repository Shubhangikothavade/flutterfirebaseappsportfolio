import 'package:flutter/material.dart';

class DetailMenu extends StatefulWidget {
  final foodName;
  final foodPrice;
  final foodImage;

  DetailMenu({this.foodName,this.foodPrice,this.foodImage});

  @override
  _DetailMenuState createState() => _DetailMenuState();
}

class _DetailMenuState extends State<DetailMenu> {
  int menuItemNumber = 0;
  var selectedCard = 'WEIGHT';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: Colors.white,
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        title: Text('Details'),
        centerTitle: true,
        actions: [
          IconButton(
              icon: Icon(Icons.more_horiz),
              onPressed: (){}),
        ],
      ),

      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
           color : Colors.transparent
           // color: Colors.red,

          ), // Main Container
          Positioned(
            top:80,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(70.0),
                color: Colors.white,
              ),
            ),
          ), //Curve Container
          Positioned(
            left: MediaQuery.of(context).size.width / 3.5,
            top:20,
            child: CircleAvatar(
              backgroundImage: AssetImage('asset/${widget.foodImage}'),
              radius: 80.0,
            ),
          ), // CircularAvatar image
          Positioned(
            top: 250,
              left: 20.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('${widget.foodName}',
                    style: TextStyle(
                      fontSize: 30.0
                    ),
                  ), //FoodName
                  SizedBox(height: 20.0,),
                  Container(
                    height: 40.0,
                    width: MediaQuery.of(context).size.width-50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('${widget.foodPrice}',
                          style: TextStyle(
                              fontSize: 20.0,
                            color: Colors.grey
                          ),
                        ),
                        VerticalDivider(
                          color: Colors.grey,
                          width: 20.0,
                          thickness: 2.0,
                        ),
                        Container(
                          width: 130.0,
                            height: 70.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              //border: Border.all(width: 1.0),
                              color: Colors.blue
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                    onTap: (){
                                      setState(() {
                                        menuItemNumber == 0 ? menuItemNumber : menuItemNumber--;
                                      });
                                    },
                                    child: Icon(Icons.remove,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text('$menuItemNumber',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.white
                                    ),),
                                  Container(
                                    width: 25.0,
                                    height: 25.0,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(1.0),
                                        border: Border.all(width: 1.0),
                                        color: Colors.white
                                    ),
                                    child: InkWell(
                                      onTap: (){
                                        setState(() {
                                          menuItemNumber++;
                                        });
                                      },
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.black,
                                        )
                                    ),
                                  )
                                ],
                              ),
                            )
                        )
                      ],
                    ),
                  ), // price,devider,additem
                  SizedBox(height: 40.0,),
                  Container(
                      height: 150.0,
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[
                          _buildInfoCard('WEIGHT', '300', 'G'),
                          SizedBox(width: 10.0),
                          _buildInfoCard('CALORIES', '267', 'CAL'),
                          SizedBox(width: 10.0),
                          _buildInfoCard('VITAMINS', 'A, B6', 'VIT'),
                          SizedBox(width: 10.0),
                          _buildInfoCard('AVAIL', 'NO', 'AV'),
                          SizedBox(width: 10.0),
                          _buildInfoCard('POTASSIUM', 'NO', 'AV'),
                          SizedBox(width: 10.0),
                          _buildInfoCard('MAGNET', 'NO', 'AV'),
                        ],
                      )
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0), bottomLeft: Radius.circular(25.0), bottomRight: Radius.circular(25.0)),
                        color: Colors.black
                    ),
                    height: 55.0,
                    width: MediaQuery.of(context).size.width-40,
                    child: Center(
                      child: Text(
                          '\$52.00',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Montserrat'
                          )
                      ),
                    ),
                  ),// Animated card
                ],
              )
          ), // For FoodName And Price
        ],
      ),
    );
  }

  Widget _buildInfoCard(String cardTitle, String info, String unit) {
    return InkWell(
        onTap: () {
          setState(() {
            selectedCard = cardTitle;
          });
        },
        child: AnimatedContainer(
            height: 100.0,
            width: 100.0,
            duration: Duration(milliseconds: 500),
            //curve: Curves.easeIn,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: cardTitle == selectedCard ? Colors.blue : Colors.white,
              border: Border.all(
                  color: cardTitle == selectedCard ?
                  Colors.transparent :
                  Colors.grey.withOpacity(0.3),
                  style: BorderStyle.solid,
                  width: 0.75
              ),
            ),

            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 15.0),
                    child: Text(cardTitle,
                        style: TextStyle(
                          fontSize: 12.0,
                          color:
                          cardTitle == selectedCard ? Colors.white : Colors.grey.withOpacity(0.7),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, bottom: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(info,
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 14.0,
                                color: cardTitle == selectedCard
                                    ? Colors.white
                                    : Colors.black,
                                fontWeight: FontWeight.bold)),
                        Text(unit,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 12.0,
                              color: cardTitle == selectedCard
                                  ? Colors.white
                                  : Colors.black,
                            ))
                      ],
                    ),
                  )
                ]
            )
        )
    );
  }

}
