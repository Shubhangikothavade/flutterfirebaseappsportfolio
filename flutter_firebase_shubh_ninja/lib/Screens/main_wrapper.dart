import 'package:flutter/material.dart';
import 'package:flutter_firebase_shubh_ninja/Models/user.dart';
import 'package:flutter_firebase_shubh_ninja/Screens/authenticate/authenticate.dart';
import 'package:flutter_firebase_shubh_ninja/Screens/home/home.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<OwnUser>(context);
    print('wrapper12 : $user');

    return user == null ? Authenticate() : Home();

  }
}
