import 'package:flutter/material.dart';
import 'package:flutter_firebase_shubh_ninja/Shared/loading.dart';
import 'package:flutter_firebase_shubh_ninja/services/auth.dart';
import 'package:flutter_firebase_shubh_ninja/Shared/constants.dart';

class SignIn extends StatefulWidget {
  final Function toggleScreen;
  SignIn({this.toggleScreen});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthService _authService = AuthService();
  final _formKey = GlobalKey<FormState>();

  String email;
  String password;
  String error = '';
  bool loading = false;

  @override
  Widget build(BuildContext context) {

    return loading ? Loading() : Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0,
        title: Text('Sign In to Brew Crew'),
        actions: [
          FlatButton.icon(
            onPressed: (){
              widget.toggleScreen();
            },
            icon: Icon(Icons.person),
            label: Text('Register')
          )
        ],
      ),
      body: Container(
          padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 20.0),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    decoration:kTextInputDecoration.copyWith(
                      hintText: 'Enter Email',
                    ),
                    validator: (value) => value.isEmpty ? 'Enter email id' : null,
                    onChanged: (value) {
                      setState(() {
                        email = value;
                      });
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    decoration:kTextInputDecoration.copyWith(
                      hintText: 'Enter password',
                    ),
                    validator: (value) => value.isEmpty ? 'Enter 6 characters in password' : null,
                    obscureText: true,
                    onChanged: (value) {
                      setState(() {
                        password = value;
                      });
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  RaisedButton(
                      child: Text('Sign In'),
                      color: Colors.pink[400],
                      onPressed: () async {
                        if(_formKey.currentState.validate())
                        {
                          setState(() {
                            loading = true;
                          });
                          dynamic result = await _authService.signInWithEmailPassword(email, password);
                          if(result == null)
                            setState(() {
                              error = 'Could Not SignIn';
                              loading = false;
                            });
                        }
                      }),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('$error'),
                ],
              ),
            ),
          )),
    );
  }
}
