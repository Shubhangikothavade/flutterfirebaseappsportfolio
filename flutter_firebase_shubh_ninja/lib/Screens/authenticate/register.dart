import 'package:flutter/material.dart';
import 'package:flutter_firebase_shubh_ninja/services/auth.dart';
import 'package:flutter_firebase_shubh_ninja/Shared/constants.dart';
import 'package:flutter_firebase_shubh_ninja/Shared/loading.dart';

class Register extends StatefulWidget {
  final Function toggleScreen;
  Register({this.toggleScreen});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final AuthService _authService = AuthService();

  final _formKey = GlobalKey<FormState>();

  String email;
  String password;
  String error = '';
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0,
        title: Text('Sign Up to Brew Crew'),
        actions: [
          FlatButton.icon(
              onPressed: (){
                widget.toggleScreen();
              },
              icon: Icon(Icons.person),
              label: Text('Sign In'),
          )
        ],
      ),
      body: Container(
          padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 20.0),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    decoration:kTextInputDecoration.copyWith(
                      hintText: 'Enter email',
                    ),
                    validator: (value) => value.isEmpty ? 'Enter email id' : null,
                    onChanged: (value) {
                      setState(() {
                        email = value;
                      });
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    decoration:kTextInputDecoration.copyWith(
                      hintText: 'Enter password',
                    ),
                    validator: (value) => value.isEmpty ? 'Enter 6 characters in password' : null,
                    obscureText: true,
                    onChanged: (value) {
                      setState(() {
                        password = value;
                      });
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  RaisedButton(
                      child: Text('Register'),
                      color: Colors.pink[400],
                      onPressed: () async {
                        if(_formKey.currentState.validate())
                          {
                            setState(() {
                              loading = true;
                            });
                            dynamic result = await _authService.registerWithEmailPassword(email, password);
                            if(result == null)
                              setState(() {
                                error = 'Please supply valid email';
                                loading = false;
                              });
                          }
                      }),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('$error'),
                ],
              ),
            ),
          )),
    );
  }
}
