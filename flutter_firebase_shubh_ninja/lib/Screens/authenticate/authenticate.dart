import 'package:flutter/material.dart';
import 'package:flutter_firebase_shubh_ninja/Screens/authenticate/register.dart';
import 'package:flutter_firebase_shubh_ninja/Screens/authenticate/sign_in.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool showSignIn = true;

  void toggleScreen(){
    setState(() {
        showSignIn = !showSignIn;
    });

  }

  @override
  Widget build(BuildContext context) {
    return showSignIn ? SignIn(toggleScreen : toggleScreen) : Register(toggleScreen : toggleScreen);
  }
}
