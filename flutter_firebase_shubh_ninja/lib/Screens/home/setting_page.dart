import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase_shubh_ninja/Models/user.dart';
import 'package:flutter_firebase_shubh_ninja/Shared/constants.dart';
import 'package:flutter_firebase_shubh_ninja/Shared/loading.dart';
import 'package:flutter_firebase_shubh_ninja/services/database.dart';
import 'package:provider/provider.dart';

class SettingsForm extends StatefulWidget {
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {

  final _formKey = GlobalKey<FormState>();
  final List<String> sugars = ['0', '1', '2', '3', '4'];

  // form values
  String _currentName;
  String _currentSugars;
  int _currentStrength;

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<OwnUser>(context);

    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context,snapshot){
        if(!snapshot.hasData)
          {
              return Loading();
          }
        UserData userdata =snapshot.data;

        return Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Text(
                'Update your brew settings.',
                style: TextStyle(fontSize: 18.0),
              ),
              SizedBox(height: 20.0),
              TextFormField(
                initialValue: userdata.name,
                decoration: kTextInputDecoration,
                validator: (val) => val.isEmpty ? 'Please enter a name' : null,
                onChanged: (val) => setState(() => _currentName = val),
              ),
              SizedBox(height: 10.0),
              DropdownButtonFormField(
                value: _currentSugars ?? userdata.sugar,
                decoration: kTextInputDecoration,
                items: sugars.map((sugar) {
                  return DropdownMenuItem(
                    value: sugar,
                    child: Text('$sugar sugars'),
                  );
                }).toList(),
                onChanged: (val){
                  setState(() {
                    _currentSugars = val;
                  });
                },
              ),
              SizedBox(height: 10.0),
              Slider(
                  value: (_currentStrength ?? userdata.strength).toDouble(),
                  min: 100,
                  max: 900,
                  activeColor: Colors.brown[_currentStrength ?? userdata.strength],
                  inactiveColor: Colors.brown[_currentStrength ?? userdata.strength],
                  divisions: 8,
                  onChanged: (val){
                    setState(() {
                      _currentStrength = val.round();
                    });
                  }
              ),
              SizedBox(height: 10.0),
              RaisedButton(
                  color: Colors.pink[400],
                  child: Text(
                    'Update',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () async {
                    if(_formKey.currentState.validate())
                      {
                        await DatabaseService(uid: user.uid).InsertOrUpdateUserData(
                         _currentSugars ?? userdata.sugar,
                         _currentName ??  userdata.name,
                         _currentStrength ?? userdata.strength);
                      }
                    Navigator.pop(context);
                  }
              ),
            ],
          ),
        );
      },

    );
  }
}