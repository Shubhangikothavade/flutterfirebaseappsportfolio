import 'package:flutter/material.dart';
import 'package:flutter_firebase_shubh_ninja/Models/brew.dart';
import 'package:provider/provider.dart';
import 'brew_tile.dart';

class BrewList extends StatefulWidget {
  @override
  _BrewListState createState() => _BrewListState();
}

class _BrewListState extends State<BrewList> {

  @override
  Widget build(BuildContext context) {
    final brews = Provider.of<List<Brew>>(context);

    int length = 0;
    if(brews != null) {
      length = brews.length;
      brews.forEach((element) {
        print(element.name);
      });
    }

    /*for(var doc in brews.docs)
      {
        print(doc.data()['Sugar']);
      } */
    return ListView.builder(
      itemCount: length,
        itemBuilder: (context,index){
            return BrewTile(brew : brews[index]);
        }
    );
  }
}
