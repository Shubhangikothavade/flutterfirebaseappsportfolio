import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase_shubh_ninja/Models/brew.dart';
import 'package:flutter_firebase_shubh_ninja/services/auth.dart';
import 'package:flutter_firebase_shubh_ninja/services/database.dart';
import 'package:provider/provider.dart';
import 'brewlist.dart';
import 'setting_page.dart';

class Home extends StatelessWidget {
  final AuthService _auth = AuthService();
  @override
  Widget build(BuildContext context) {

    void _showSettingsPanel(){
      showModalBottomSheet(
        context: context,
        builder: (context){
          return Container(
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
            child: SettingsForm(),
          );
        }
      );

    }

    return StreamProvider<List<Brew>>.value(
      value: DatabaseService().brews,

      child: Scaffold(
        backgroundColor: Colors.brown[100],
        appBar: AppBar(
          backgroundColor: Colors.brown[400],
          title: Text('Brew Crew'),
          actions: [
            FlatButton.icon(
                onPressed: () async{
                  await  _auth.signOut();
                 // DatabaseService().getData();
                },
                icon: Icon(Icons.person),
                label: Text('logout'),
            ),
            FlatButton.icon(
                onPressed: (){
                  _showSettingsPanel();
                },
                icon: Icon(Icons.settings),
                label: Text('Setting')
            )
          ],
        ),
        body: BrewList(),
      ),
    );
  }
}
