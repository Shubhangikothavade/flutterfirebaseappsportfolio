import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_firebase_shubh_ninja/Models/brew.dart';
import 'package:flutter_firebase_shubh_ninja/Models/user.dart';

class DatabaseService{

  final uid;
  DatabaseService({this.uid});

  final brewCollection = FirebaseFirestore.instance.collection('Brew');

  Future InsertOrUpdateUserData(String sugar, String name, int strength) async{
    return await brewCollection.doc(uid).set({
      'Sugar' : sugar,
      'Name' : name,
      'Strength' : strength,
    });
  }

  // brew list from snapshot
  List<Brew> _brewListFromSnapshot(QuerySnapshot snapshot)
  {
    List<Brew> _brewList = [];
    for(var doc in snapshot.docs)
      {
         _brewList.add(Brew(
          sugar: doc.data()['Sugar'] ?? '0',
          name: doc.data()['Name'] ?? '',
          strength: doc.data()['Strength'] ?? 0,
        ));
      }
    if(_brewList != null)
      return _brewList;

    // upper and below blocks are correct
    /*return snapshot.docs.map((doc){
      return Brew(
        sugar: doc.data()['Sugar'] ?? '0',
        name: doc.data()['Name'] ?? '',
        strength: doc.data()['Strength'] ?? 0,
      );
    }).toList();
*/
  }

  // get brew stream
  /*Stream<QuerySnapshot> get brews {
    return brewCollection.snapshots();
  }*/

// get brew stream from snapshot
  Stream<List<Brew>> get brews {
    return brewCollection.snapshots()
        .map(_brewListFromSnapshot);
  }

  void getData() async
  {
    final result = await brewCollection.get();
    print('getData = ${result.docs}');
    for(var doc in result.docs){
      print('doc = ${doc.data()['Name']}');
    }
  }


  // convert to userdata from documentsnapshot
  UserData _userDataFromDocumentSnapshot(DocumentSnapshot snapshot){
    return UserData(
      uid: uid,
      name: snapshot.data()['Name'],
      strength: snapshot.data()['Strength'],
      sugar: snapshot.data()['Sugar'],
    );

}
  Stream<UserData> get userData{
    return brewCollection.doc(uid).snapshots()
        .map(_userDataFromDocumentSnapshot);
  }
}