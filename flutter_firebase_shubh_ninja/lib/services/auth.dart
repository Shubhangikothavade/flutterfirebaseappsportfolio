import 'dart:ffi';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_firebase_shubh_ninja/Models/user.dart';
import 'package:flutter_firebase_shubh_ninja/services/database.dart';

class AuthService{
  final _auth = FirebaseAuth.instance;

  // auth changes user stream
 /* Stream<User> get ownuser{
    return _auth.authStateChanges();
  } */

  Stream<OwnUser> get ownuser{
    return _auth.authStateChanges()
    .map(_UserFromFireBaseUser);
  }

  // Create user object based on firebase user
  OwnUser _UserFromFireBaseUser(User user)
  {
    return user != null ? OwnUser(uid: user.uid) : null;
  }

  // signin anon
  Future signInAnon() async {
    try {
     final result = await _auth.signInAnonymously();
     User user = result.user;
     return _UserFromFireBaseUser(user);
    }

    catch(e){
      print('catch ${e.toString()}');
      return null;
    }
  }
  // signIn email n password
  Future signInWithEmailPassword(String email, String password) async
  {
    try {
      final result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User user = result.user;
      return _UserFromFireBaseUser(user);
    }
    catch(e){
      print('catch ${e.toString()}');
      return null;
    }
  }

  // register email n password
  Future registerWithEmailPassword(String email, String password) async
  {
    try {
      final result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User user = result.user;

      // create new document for new user
      await DatabaseService(uid:  user.uid).InsertOrUpdateUserData('2', 'new member', 100);
      return _UserFromFireBaseUser(user);
    }
    catch(e){
      print('catch ${e.toString()}');
      return null;
    }
  }

  // signout
  Future signOut() async{
    try {
      return await _auth.signOut();
    }
    catch(e){
      print('sign Out : ${e.toString()}');
      return null;
    }
  }

}