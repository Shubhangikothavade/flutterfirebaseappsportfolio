class Brew{
  String name;
  String sugar;
  int strength;

  Brew({this.sugar, this.name, this.strength});
}