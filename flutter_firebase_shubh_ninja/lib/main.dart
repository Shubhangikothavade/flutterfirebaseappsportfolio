import 'package:flutter/material.dart';
import 'package:flutter_firebase_shubh_ninja/Models/user.dart';
import 'package:flutter_firebase_shubh_ninja/Screens/main_wrapper.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_firebase_shubh_ninja/services/auth.dart';
import 'package:provider/provider.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(StreamProvider<OwnUser>.value(
    value: AuthService().ownuser,
    child: MaterialApp(
      home: Wrapper(),
    ),
  ));
}
