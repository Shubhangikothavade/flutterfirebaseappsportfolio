import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pass_data_one_page_to_other/Services/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class PackageDemoClass extends StatefulWidget {
  @override
  _PackageDemoClassState createState() => _PackageDemoClassState();
}

class _PackageDemoClassState extends State<PackageDemoClass> {
  String time ='loading..';
  void getWorldTime() async{
    ShowWorldTime instance = ShowWorldTime(flag:'flag.png',url:'Europe/London');
    await instance.getTime();

     Navigator.pushReplacementNamed(context, '/home',arguments:{
      'flag' : instance.flag,
      'time' : instance.time,
       'isDayTime' : instance.isDayTime,
    });
  }

  void getData() async
  {
    String launchurl = 'https://jsonplaceholder.typicode.com/todos/1';
    if(await canLaunch(launchurl))
    {
      print('url launched');
      launch(launchurl);
      //Map data = jsonDecode(response.body);
      //print(data[title]);
    }
    else
    {
      print('url not launched');
    }
  }

  @override
  void initState() {
    super.initState();
    //print('PackageDemo init state function');
    //getData();
    getWorldTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
        appBar: AppBar(
          title: Text('Welcome to Package page'),
        ),
        body: SpinKitRotatingCircle(
          color: Colors.white,
          size: 50.0,
    ));
  }
}
