import 'package:flutter/material.dart';

class AsyncDemo extends StatefulWidget {
  @override
  _AsyncDemoState createState() => _AsyncDemoState();
}

class _AsyncDemoState extends State<AsyncDemo> {

  // Asynchronus code
  void getData() async {
    String name1 = await Future.delayed(Duration(seconds: 3),(){
      return 'Hi Shubhangi 3';
    });

    String name2 = await Future.delayed(Duration(seconds: 2),(){
      return 'Hi Shubhangi 2';
    });
    print('Hi $name1 & $name2');
  }
  /*// synchronus code
  void getData(){
     Future.delayed(Duration(seconds:3),(){
      print('Hi Shubhangi 3');
    });

    Future.delayed(Duration(seconds: 2),(){
      print('Hi shubhangi 2');
    });
    print('hi shubhangi');
  } */

  int counter = 0;
  @override
  void initState() {
    super.initState();
    //print('asysc init state function');
    getData();
  }

  @override
  Widget build(BuildContext context) {
    //print('async build');
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome to Async page'),
      ),
      body: RaisedButton(
        onPressed: (){
          setState(() {
            counter += 1;
          });
        },
        child: Text('counter $counter'),
      ),
    );
  }
}
