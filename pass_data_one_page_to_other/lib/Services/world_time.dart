import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class ShowWorldTime{
  String url;
  String time;
  String flag;
  bool isDayTime;

  ShowWorldTime({this.flag,this.url});

  Future<void> getTime() async
  {
    Response response = await get('http://worldtimeapi.org/api/timezone/$url');
    Map data = jsonDecode(response.body);
    //print(data['datetime']);
    String datetime = data['datetime'];

    //Substring inclusive first index, exclusive last index
    String offset = data['utc_offset'].substring(1,3);
    DateTime now = DateTime.parse(datetime);
    //print('datetime = $now');
    //print('offset = $offset');
    now = now.add(Duration(hours: int.parse(offset)));
    //print('now = $now');

    // check time is day time or night time
    isDayTime = now.hour > 6 && now.hour <11 ? true : false;

    time = DateFormat.jm().format(now);
    print('time = $time');
  }
}