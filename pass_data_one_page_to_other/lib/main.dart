import 'package:flutter/material.dart';
import 'package:pass_data_one_page_to_other/Pages/asyncPage.dart';
import 'package:pass_data_one_page_to_other/Pages/Home.dart';
import 'package:pass_data_one_page_to_other/Pages/PackageDemo.dart';

// 1. pass data from one page to other..
// 2. loading page spinner..
// 3. extract time from datetime package
void main() {
  runApp(MaterialApp(
    initialRoute: '/package',
    routes: {
      '/home' : (context) => Home(),
      '/async' : (context) => AsyncDemo(),
      '/package': (context) => PackageDemoClass()
    },
  ));
}